package com.kaributechs.claimsettlement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClaimSettlementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClaimSettlementApplication.class, args);
	}

}

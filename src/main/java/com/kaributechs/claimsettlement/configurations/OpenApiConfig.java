package com.kaributechs.claimsettlement.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "Policy && Claims Management",
                description = "" +
                        "This is an api documentation page for Policy && Claims Management developed by Kaributechs",
                contact = @Contact(
                        name = "Tinashe Makara && Wellington Mpofu",
                        email = "makaratinashe22@gmail.com"
                )
        ),
        servers = @Server(url = "http://insurehubbackend.southafricanorth.cloudapp.azure.com:7012")
)
public class OpenApiConfig {
}

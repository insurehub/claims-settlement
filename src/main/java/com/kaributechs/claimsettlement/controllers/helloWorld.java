package com.kaributechs.claimsettlement.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/hello")
public class helloWorld {
    @GetMapping
    public String sayHi(){
        return "Hello SpringBoot";
    }
}

FROM openjdk:11
MAINTAINER wellington.t.mpofu@gmail.com
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} claim-settlement.jar
ENTRYPOINT ["java","-jar","/claim-settlement.jar"]